﻿// ReSharper disable InconsistentNaming

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Context
{
    public class EFContext : DbContext
    {
        public EFContext()
            :base("ConnectionString")
        {
            
        }

        public DbSet<Cliente> Clientes { get; set; }

        public DbSet<Vendedor> Vendedores { get; set; }

        public DbSet<Produto> Produtos { get; set; }

        public DbSet<FormaDePagamento> FormasDePagamento { get; set; }

        public DbSet<Venda> Vendas { get; set; }

        public DbSet<ProdutoVenda> ProdutosVenda { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            modelBuilder.Properties()
                // ReSharper disable once PossibleNullReferenceException
                .Where(p => p.Name == p.ReflectedType.Name + "Id")
                .Configure(p => p.IsKey());

            modelBuilder.Properties<string>()
                .Configure(p => p.HasColumnType("varchar"));

            modelBuilder.Properties<string>()
                .Configure(p => p.HasMaxLength(100));

            base.OnModelCreating(modelBuilder);
        }
    }
}