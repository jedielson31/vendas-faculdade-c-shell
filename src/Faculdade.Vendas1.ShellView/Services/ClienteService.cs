﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository;
using Faculdade.Vendas2.ShellView.Services.Common;
using Faculdade.Vendas2.ShellView.Services.Interfaces;

namespace Faculdade.Vendas2.ShellView.Services
{
    public class ClienteService : IClienteService
    {
        private readonly ClienteRepository _clienteRepository;

        public ClienteService()
        {
            _clienteRepository = new ClienteRepository();
        }

        public ServiceOperationResult Create(Cliente cliente)
        {
            var result = new ServiceOperationResult();
            try
            {
                if (ExisteComOMesmoCpf(cliente))
                {
                    result.AddErro("Já existe um cliente com o mesmo cpf.");
                    return result;
                }
                _clienteRepository.Create(cliente);

                return result;
            }
            catch (Exception ex)
            {
                result.AddErro(ex.Message);
                return result;
            }
        }

        public IEnumerable<Cliente> All()
        {
            return _clienteRepository.All();
        }

        public bool ExisteComOMesmoCpf(Cliente cliente)
        {
            if (string.IsNullOrEmpty(cliente?.Cpf))
            {
                throw new ArgumentException("Cliente invalido. Nulo ou sem CPF");
            }

            return All().Any(item => item.Cpf == cliente.Cpf);
        }
    }
}