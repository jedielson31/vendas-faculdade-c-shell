﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository;
using Faculdade.Vendas2.ShellView.Services.Common;
using Faculdade.Vendas2.ShellView.Services.Interfaces;

namespace Faculdade.Vendas2.ShellView.Services
{
    public class VendedorService : IVendedorService
    {

        private readonly VendedorRepository _repository;

        public VendedorService()
        {
            _repository = new VendedorRepository();
        }

        public IEnumerable<Vendedor> All()
        {
            return _repository.All();
        }

        public ServiceOperationResult Create(Vendedor entity)
        {
            var result = new ServiceOperationResult();

            try
            {
                if (entity == null)
                {
                    throw new Exception("A entidade não pode ser nula");
                }

                if (PossuiComMesmoCpf(entity))
                {
                    result.AddErro("Já existe um vendedor com este CPF");
                    return result;
                }

                _repository.Create(entity);
                return result;
            }
            catch (Exception ex)
            {
                result.AddErro(ex.Message);
                return result;
            }
        }

        public bool PossuiComMesmoCpf(Vendedor entity)
        {
            return _repository.All().Any(x => x.Cpf == entity.Cpf);
        }
    }
}