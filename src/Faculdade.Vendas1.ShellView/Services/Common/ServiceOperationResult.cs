﻿using System.Collections.Generic;
using System.Linq;

namespace Faculdade.Vendas2.ShellView.Services.Common
{
    public class ServiceOperationResult
    {
        public ServiceOperationResult()
        {
            Erros = new List<string>();
        }

        public List<string> Erros { get; }

        public bool Success => !Erros.Any();

        public void AddErro(string message)
        {
            if (message == null || message.Trim() == string.Empty)
            {
                return;
            }

            Erros.Add(message);
        }
    }
}