﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository;
using Faculdade.Vendas2.ShellView.Services.Common;
using Faculdade.Vendas2.ShellView.Services.Interfaces;

namespace Faculdade.Vendas2.ShellView.Services
{
    public class FormaDePagamentoService : IFormaDePagamentoService
    {
        private readonly FormaDePagamentoRepository _repository;

        public FormaDePagamentoService()
        {
            _repository = new FormaDePagamentoRepository();
        }

        public ServiceOperationResult Create(FormaDePagamento formaDePagamento)
        {
            var result = new ServiceOperationResult();
            try
            {
                if (ExisteComOMesmoNome(formaDePagamento))
                {
                    result.AddErro("Já existe uma forma de pagamento com o mesmo nome.");
                    return result;
                }
                _repository.Create(formaDePagamento);

                return result;
            }
            catch (Exception ex)
            {
                result.AddErro(ex.Message);
                return result;
            }
        }

        public IEnumerable<FormaDePagamento> All( )
        {
            return _repository.All();
        }

        public bool ExisteComOMesmoNome(FormaDePagamento formaDePagamento)
        {
            if (string.IsNullOrEmpty(formaDePagamento?.Nome))
            {
                throw new ArgumentException("Forma de pagamento invalida. Nula ou sem Nome");
            }

            return _repository.All().Any(x => x.Nome.ToUpper() == formaDePagamento.Nome.ToUpper());
        }
    }
}