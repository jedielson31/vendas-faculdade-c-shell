﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository;
using Faculdade.Vendas2.ShellView.Services.Common;
using Faculdade.Vendas2.ShellView.Services.Interfaces;

namespace Faculdade.Vendas2.ShellView.Services
{
    public class ProdutoService : IProdutoService
    {
        private readonly ProdutoRepository _repository;
        public ProdutoService()
        {
            _repository = new ProdutoRepository();
        }

        public IEnumerable<Produto> All()
        {
            return _repository.All();
        }

        public ServiceOperationResult Create(Produto entity)
        {
            var result = new ServiceOperationResult();

            try
            {
                if (PossuiComMesmoNome(entity))
                {
                    result.AddErro("Já existe um produto cadastrado com este nome.");
                    return result;
                }

                _repository.Create(entity);
                return result;
            }
            catch (Exception ex)
            {
                result.AddErro(ex.Message);
                return result;
            }
        }

        public bool PossuiComMesmoNome(Produto entity)
        {
            return _repository.All().Any(x => x.Nome.ToUpper() == entity.Nome.ToUpper());
        }
    }
}