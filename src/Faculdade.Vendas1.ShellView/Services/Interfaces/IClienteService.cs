﻿using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Services.Interfaces
{
    public interface IClienteService : IService<Cliente>
    {
        bool ExisteComOMesmoCpf(Cliente cliente);
    }
}