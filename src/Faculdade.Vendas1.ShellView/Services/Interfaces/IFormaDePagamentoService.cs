﻿using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Services.Interfaces
{
    public interface IFormaDePagamentoService
    {
        bool ExisteComOMesmoNome(FormaDePagamento formaDePagamento);
    }
}