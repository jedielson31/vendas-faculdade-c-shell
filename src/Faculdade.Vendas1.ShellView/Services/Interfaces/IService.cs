﻿using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Services.Common;

namespace Faculdade.Vendas2.ShellView.Services.Interfaces
{
    public interface IService<TEntity>
    {
        IEnumerable<TEntity> All();
        ServiceOperationResult Create(TEntity entity);
    }
}