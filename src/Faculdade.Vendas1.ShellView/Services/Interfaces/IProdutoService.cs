﻿using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Services.Interfaces
{
    public interface IProdutoService : IService<Produto>
    {
        bool PossuiComMesmoNome(Produto entity);
    }
}