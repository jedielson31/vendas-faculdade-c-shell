﻿using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Services.Interfaces
{
    public interface IVendedorService : IService<Vendedor>
    {
        bool PossuiComMesmoCpf(Vendedor entity);
    }
}