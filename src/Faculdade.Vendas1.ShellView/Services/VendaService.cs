﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository;
using Faculdade.Vendas2.ShellView.Services.Common;
using Faculdade.Vendas2.ShellView.Services.Interfaces;

namespace Faculdade.Vendas2.ShellView.Services
{
    public class VendaService : IVendaService
    {
        private readonly VendaRepository _repository;

        public VendaService()
        {
            _repository = new VendaRepository();
        }

        public IEnumerable<Venda> All()
        {
            return _repository.All();
        }

        public ServiceOperationResult Create(Venda entity)
        {
            var result = new ServiceOperationResult();

            try
            {
                if (entity == null)
                {
                    throw new Exception("A venda não pode ser nula");
                }

                _repository.Create(entity);

                return result;
            }
            catch (Exception ex)
            {
                result.AddErro(ex.Message);
                return result;
            }
        }
    }
}