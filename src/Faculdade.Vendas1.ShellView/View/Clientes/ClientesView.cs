﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Controller;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.View.Clientes
{
    public class FormularioCadastroClientes : IFomularioCadastro<Cliente>
    {
        public Cliente Cliente { get; private set; }

        public Cliente Cadastrar()
        {
            Cliente = new Cliente
            {
                Nome = LeNome(),
                Cpf = LeCpf()
            };

            Console.Clear();
            return Cliente;
        }

        public Cliente Cadastrar(Cliente cliente)
        {
            if (cliente == null)
            {
                cliente = new Cliente();
            }

            cliente.Nome = LeNome(cliente.Nome);
            cliente.Cpf = LeCpf(cliente.Cpf);

            Console.Clear();
            return Cliente;
        }

        private  string LeCpf(string valor = "")
        {
            return this.LeString("Por favor, insira o CPF do cliente", "O cpf informado é inválido. Por favor, tente novamente", valor);

        }

        public string LeNome(string valor = "")
        {
            return this.LeString("Informe o Nome do Cliente", "O valor informado é inválido. Por favor, tente novamente", valor);
        }

    }

    public interface IFomularioCadastro<out TEntity> : ITela
    {
        TEntity Cadastrar();
    }

    public class ListagemClientes : IListagem<Cliente>
    {
        public void Listar(IEnumerable<Cliente> items)
        {
            if(items == null) return;
            foreach (var cliente in items)
            {
                this.Imprime("Cliente[Id:{0}; Nome:{1}; Cpf:{2}]",cliente.ClienteId, cliente.Nome, cliente.Cpf);
            }
            this.Imprime("Pressione Qualquer tecla para voltar");
            Console.ReadKey();
            Console.Clear();
        }
    }

    public interface IListagem<in TEntity> : ITela
    {
        void Listar(IEnumerable<TEntity> items);
    }
}