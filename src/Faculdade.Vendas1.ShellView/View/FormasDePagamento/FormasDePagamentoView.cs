﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.View.Clientes;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.View.FormasDePagamento
{
    public class FormularioDeCadastroFormasDePagamento : IFomularioCadastro<FormaDePagamento>
    {
        public FormaDePagamento Cadastrar()
        {
            var forma = new FormaDePagamento
            {
                Nome = LeNome()
            };

            return forma;
        }

        public FormaDePagamento Cadastrar(FormaDePagamento forma)
        {
            if (forma == null)
            {
                forma = new FormaDePagamento();
            }

            forma.Nome = LeNome(forma.Nome);

            return forma;
        }


        private string LeNome(string valor = "")
        {
            return this.LeString("Informe o nome da forma de pagamento", "O nome informado é inválido. Tente novamente",
                valor);
        }
    }

    public class ListagemFormaDePagamento : IListagem<FormaDePagamento>
    {
        public void Listar(IEnumerable<FormaDePagamento> items)
        {
            var formasDePagamento = items as FormaDePagamento[] ?? items.ToArray();
            if (items == null || !formasDePagamento.Any())
            {
                this.Imprime("Não há formas de pagamento registradas");
                return;
            }

            foreach (var formaDePagamento in formasDePagamento)
            {
                this.Imprime("Forma De Pagamento[Id: {0}; Nome: {1}]",
                    formaDePagamento.FormaDePagamentoId,
                    formaDePagamento.Nome);
            }
            this.Imprime("Pressione Qualquer tecla para voltar");
            Console.ReadKey();
            Console.Clear();
        }
    }
}