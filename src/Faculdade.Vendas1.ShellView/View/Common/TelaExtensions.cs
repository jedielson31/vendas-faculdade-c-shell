﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Forms;
using Faculdade.Vendas2.ShellView.Controller;
using Faculdade.Vendas2.ShellView.Services.Common;

namespace Faculdade.Vendas2.ShellView.View.Common
{
    public static class TelaExtensions
    {
        public static string LeString(this ITela tela, string message, string errorMessage, string valor = "")
        {
            Imprime(tela, message);
            if (!string.IsNullOrEmpty(valor))
            {
                SendKeys.SendWait(valor);
            }
            var value = Console.ReadLine();

            while (string.IsNullOrEmpty(value))
            {
                ImprimeErro(tela, errorMessage);
                Imprime(tela, message);
                if (!string.IsNullOrEmpty(valor))
                {
                    SendKeys.SendWait(valor);
                }
                value = Console.ReadLine();
            }

            return value;
        }

        public static int LeInteiro(this ITela tela, string message, string errorMessage, int? valor = null)
        {
            var strRetorno = LeString(tela, message, errorMessage, valor == null ? string.Empty : valor.ToString());
            int aux;

            while (!int.TryParse(strRetorno, out aux))
            {
                strRetorno = LeString(tela, message, errorMessage, valor == null ? string.Empty : valor.ToString());
            }

            return aux;
        }

        public static string LeOpcoes(this ITela tela, string message, string errorMessage, List<string> opcoesValidas,
            string valor = "")
        {
            Imprime(tela, message);
            if (!string.IsNullOrEmpty(valor))
            {
                SendKeys.SendWait(valor);
            }
            var value = Console.ReadLine();

            while (string.IsNullOrEmpty(value) || opcoesValidas.All(x => x.ToUpper() != value.ToUpper()))
            {
                ImprimeErro(tela, errorMessage);
                Imprime(tela, message);
                if (!string.IsNullOrEmpty(valor))
                {
                    SendKeys.SendWait(valor);
                }
                value = Console.ReadLine();
            }

            return value;

        }

        public static decimal LeDecimal(this ITela tela, string message, string errorMessage, decimal? valorInicial = null)
        {
            var valor = LeString(tela, message, errorMessage, valorInicial == null ? string.Empty : valorInicial.ToString());
            decimal aux;

            while (!decimal.TryParse(valor, out aux))
            {
                valor = LeString(tela, message, errorMessage, valor);
            }

            return aux;
        }

        public static DateTime LeData(this ITela tela, string message, string errorMessage,
            DateTime? valorInicial = null)
        {
            var data = LeString(tela, message, errorMessage, (valorInicial != null ? valorInicial.ToString() : string.Empty));
            DateTime aux;

            var formats = new string[] { "dd/MM/yyyy", "dd/MM/yy" };
            while (!DateTime.TryParseExact(data, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out aux))
            {
                ImprimeErro(tela, "O valor não é uma data válida. Por favor, tente novamente.");
                data = LeString(tela, message, errorMessage, valorInicial.ToString());
            }

            return aux;
        }
        public static void ImprimeResultadoComErros(this ITela tela, ServiceOperationResult result)
        {
            if (result == null)
            {
                return;
            }

            foreach (var erro in result.Erros)
            {
                tela.ImprimeErro(erro);
            }
        }

        #region Printers
        public static void Imprime(this ITela tela, string erro, params object[] data)
        {
            Imprime(tela, erro, false, data);
        }

        public static void Imprime(this ITela tela, string erro, bool clearConsole, params object[] data)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(erro, data);
            if (clearConsole)
            {
                Console.Clear();
            }
        }

        public static void ImprimeErro(this ITela tela, string erro, params object[] data)
        {
            ImprimeErro(tela, erro, false, data);
        }

        public static void ImprimeErro(this ITela tela, string erro, bool clearConsole, params object[] data)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(erro, data);
            Console.ForegroundColor = ConsoleColor.White;

            if (clearConsole)
            {
                Console.Clear();
            }
        }

        public static void ImprimeAlerta(this ITela tela, string erro, params object[] data)
        {
            ImprimeAlerta(tela, erro, false, data);
        }

        public static void ImprimeAlerta(this ITela tela, string erro, bool clearConsole, params object[] data)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(erro, data);
            Console.ForegroundColor = ConsoleColor.White;

            if (clearConsole)
            {
                Console.Clear();
            }
        }

        public static void ImprimeSucesso(this ITela tela, string erro, params object[] data)
        {
            ImprimeSucesso(tela, erro, false, data);
        }

        public static void ImprimeSucesso(this ITela tela, string erro, bool clearConsole, params object[] data)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(erro, data);
            Console.ForegroundColor = ConsoleColor.White;

            if (clearConsole)
            {
                Console.Clear();
            }
        }
        #endregion
    }


}
