﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.View.Clientes;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.View.Produtos
{
    public class FormularioCadastroProduto : IFomularioCadastro<Produto>
    {
        public Produto Cadastrar()
        {
            var produto = new Produto
            {
                Nome = LeNome(),
                PrecoDeCompra = LePrecoDeCompra(),
                Markup = LeMarkup()
            };

            return produto;
        }

        public Produto Cadastrar(Produto produto)
        {
            if (produto == null)
            {
                produto = new Produto();
            }

            produto.Nome = LeNome(produto.Nome);
            produto.PrecoDeCompra = LePrecoDeCompra(produto.PrecoDeCompra);
            produto.Markup = LeMarkup(produto.Markup);
            return produto;
        }

        private decimal LeMarkup(decimal valor = decimal.Zero)
        {
            valor = valor * 100;
            return this.LeDecimal("Por favor, digite o percentual do markup, separando decimais com virgula. Ex: 30,35 (retorna 30.35%)", "O valor informado é inválido.  Por favor, tente novamente", valor) / 100;
        }

        private decimal LePrecoDeCompra(decimal valor = decimal.Zero)
        {
            return this.LeDecimal("Por favor, digite o preco de compra, separando decimais com virgula", "O valor informado é inválido.  Por favor, tente novamente", valor);
        }

        private string LeNome(string valor = "")
        {
            return this.LeString("Informe o Nome do Produto", "O valor informado é inválido. Por favor, tente novamente", valor);
        }
    }

    public class ListagemProdutos : IListagem<Produto>
    {
        public void Listar(IEnumerable<Produto> items)
        {
            var produtos = items as Produto[] ?? items.ToArray();
            if (items == null || !produtos.Any())
            {
                this.Imprime("Não há produtos registrados");
            }

            foreach (var produto in produtos)
            {
                this.Imprime("Produto:[Id:{0}; Nome:{1}; Preço de Compra: R$ {2}; Markup:{3} %]", produto.ProdutoId, produto.Nome, produto.PrecoDeCompra, produto.Markup * 100);
            }

            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Console.Clear();
        }
    }
}