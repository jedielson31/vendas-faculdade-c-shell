﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Controller;
using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.View.Clientes;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.View.Venda
{
    public class FormularioCadastroVendas : IFomularioCadastro<Domain.Venda>
    {
        private readonly IList<Cliente> _clientes;
        private readonly IList<Domain.Vendedor> _vendedores;
        private readonly IList<Produto> _produtos;
        private readonly IList<FormaDePagamento> _formasDePagamento;

        public FormularioCadastroVendas(IList<Cliente> clientes, IList<Domain.Vendedor> vendedores, IList<Produto> produtos, IList<FormaDePagamento> formas)
        {
            _clientes = clientes;
            _vendedores = vendedores;
            _produtos = produtos;
            _formasDePagamento = formas;
        }

        public Domain.Venda Cadastrar()
        {
            var venda = new Domain.Venda
            {
                Cliente = this.SelecionarCliente(_clientes),
                Vendedor = this.SelecionarVendedor(_vendedores),
                FormaDePagamento = this.SelecionarFormaDePagamento(_formasDePagamento),
                DataDaVenda = DateTime.Now,
                ProdutosDaVenda = new List<ProdutoVenda>()
            };

            var inserirProduto = "S";

            while (inserirProduto.ToUpper() == "S")
            {
                var produtoVenda = new ProdutoVenda()
                {
                    Produto = this.SelecionarProduto(_produtos),
                    Quantidade = this.LeInteiro("Qual a quantidade deste produto", "O valor é inválido. Por favor, tente novamente")
                };
                venda.ProdutosDaVenda.Add(produtoVenda);
                inserirProduto = this.LeOpcoes("Deseja informar um novo produto (s/n)?", "Opção Inválida. Tente Novamente",
                    new List<string> { "S", "N" });
            }

            return venda;
        }

        public List<Domain.Venda> CadastrarVendas()
        {
            var retorno = new List<Domain.Venda>();
            var teste = "S";
            while (teste.ToUpper() == "S")
            {
                retorno.Add(Cadastrar());
                teste = this.LeOpcoes("Deseja adicionar uma nova venda? (s/n)", "Valor inválido. Tente novamente", new List<string> { "S", "N" });
            }

            return retorno;
        }
    }

    public class ListagemVendas : IListagem<Domain.Venda>
    {
        public void Listar(IEnumerable<Domain.Venda> items)
        {
            var vendas = items as Domain.Venda[] ?? items.ToArray();

            if (!vendas.Any())
            {
                this.Imprime("Não há vendas cadastradas.");
            }

            foreach (var venda in vendas)
            {
                this.Imprime("Venda[Id:{0}; Cliente:{1}; Data:{2:dd/MM/yyyy}; Forma de Pagamento: {3}; Vendedor:{4}; Total: R$ {5}",
                    venda.VendaId,
                    venda.Cliente.Nome,
                    venda.DataDaVenda,
                    venda.FormaDePagamento.Nome,
                    venda.Vendedor.Nome,
                    Math.Round(venda.ProdutosDaVenda.Sum(x => x.SubTotal), 2));
                foreach (var produtoVenda in venda.ProdutosDaVenda)
                {
                    this.Imprime("\t[Produto: {0}; Valor Unitario: R$ {1}; Quantidade: {2}; SubTotal: R$ {3}]",
                        produtoVenda.Produto.Nome,
                        produtoVenda.Produto.ValorUnitario,
                        produtoVenda.Quantidade,
                        produtoVenda.SubTotal);
                }
                this.Imprime("]");
                this.Imprime("----------------------------------");
            }

            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Console.Clear();
        }
    }

    public static class VendasViewUtils
    {
        public static Produto SelecionarProduto(this ITela tela, IList<Produto> produtos)
        {
            ExibeListaDeProdutos(tela, produtos);
            var opcoes = produtos.Select(x => x.ProdutoId.ToString()).ToList();
            var opcao = tela.LeOpcoes("Por Favor, informe o Id do produto desejado", "Produto Inválido. Tente Novamente.", opcoes);

            return produtos.FirstOrDefault(x => x.ProdutoId.ToString() == opcao);
        }

        public static void ExibeListaDeProdutos(this ITela tela, IList<Produto> produtos)
        {
            tela.ImprimeAlerta("Produtos:");
            foreach (var produto in produtos)
            {
                tela.ImprimeSucesso("{0} - {1}. Valor {2}", produto.ProdutoId, produto.Nome, produto.PrecoDeCompra + produto.Markup);
            }
        }

        public static Domain.Vendedor SelecionarVendedor(this ITela tela, IList<Domain.Vendedor> vendedores)
        {
            tela.ImprimeAlerta("Vendedor:");
            foreach (var vendedor in vendedores)
            {
                tela.ImprimeSucesso("{0} - {1}", vendedor.VendedorId, vendedor.Nome);
            }
            var opcoes = vendedores.Select(x => x.VendedorId.ToString()).ToList();
            var opcao = tela.LeOpcoes("Por Favor, informe o Id do vendedor", "Vendedor Inválido. Tente Novamente.", opcoes);

            return vendedores.FirstOrDefault(x => x.VendedorId.ToString() == opcao);
        }

        public static Cliente SelecionarCliente(this ITela tela, IList<Cliente> clientes)
        {
            tela.ImprimeAlerta("Clientes:");
            foreach (var cliente in clientes)
            {
                tela.ImprimeSucesso("{0} - {1}", cliente.ClienteId, cliente.Nome);
            }
            var opcoes = clientes.Select(x => x.ClienteId.ToString()).ToList();
            var opcao = tela.LeOpcoes("Por Favor, informe o Id do cliente", "Cliente Inválido. Tente Novamente.", opcoes);

            return clientes.FirstOrDefault(x => x.ClienteId.ToString() == opcao);
        }

        public static FormaDePagamento SelecionarFormaDePagamento(this ITela tela, IList<FormaDePagamento> formas)
        {
            tela.ImprimeAlerta("Formas de Pagamento:");
            foreach (var forma in formas)
            {
                tela.ImprimeSucesso("{0} - {1}", forma.FormaDePagamentoId, forma.Nome);
            }
            var opcoes = formas.Select(x => x.FormaDePagamentoId.ToString()).ToList();
            var opcao = tela.LeOpcoes("Por Favor, informe o Id da forma de pagamento", "Forma de Pagamento Inválida. Tente Novamente.", opcoes);

            return formas.FirstOrDefault(x => x.FormaDePagamentoId.ToString() == opcao);
        }

        public static IList<Cliente> SelecionarListaDeClientes(this ITela tela, IList<Cliente> clientes)
        {
            var retorno = new List<Cliente>();

            var selecionarNovo = "S";
            while (selecionarNovo.ToUpper() == "S")
            {
                retorno.Add(SelecionarCliente(tela, clientes));
                selecionarNovo = tela.LeOpcoes("Deseja adicionar um novo cliente (s/n)",
                    "Opção inválida. Tente novamente", new List<string> { "S", "N" });
            }

            return retorno;
        }
    }
}