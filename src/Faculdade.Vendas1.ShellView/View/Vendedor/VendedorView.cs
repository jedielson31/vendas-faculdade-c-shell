using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.View.Clientes;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.View.Vendedor
{
    public class FormularioCadastroVendedor : IFomularioCadastro<Domain.Vendedor>
    {
        public Domain.Vendedor Cadastrar()
        {
            return new Domain.Vendedor()
            {
                Nome = LeNome(),
                Cpf = LeCpf(),
                TaxaDeComissao = LeTaxaDeComissao()
            };
        }

        public Domain.Vendedor Cadastrar(Domain.Vendedor vendedor)
        {
            if (vendedor == null)
            {
                vendedor = new Domain.Vendedor();
            }

            vendedor.Nome = LeNome(vendedor.Nome);
            vendedor.Cpf = LeCpf(vendedor.Cpf);
            vendedor.TaxaDeComissao = LeTaxaDeComissao(vendedor.TaxaDeComissao);

            return vendedor;

        }

        private double LeTaxaDeComissao(double valor = 0)
        {
            valor = valor * 100;
            return ((double)this.LeDecimal("Por favor, digite o percentual da taxa de comiss�o, separando decimais com virgula. Ex: 30,35 (retorna 30.35%)", "O valor informado � inv�lido.  Por favor, tente novamente", (decimal)valor)) / 100;
        }

        private string LeCpf(string valor = "")
        {
            return this.LeString("Por favor, insira o CPF do vendedor", "O cpf informado � inv�lido. Por favor, tente novamente", valor);

        }

        public string LeNome(string valor = "")
        {
            return this.LeString("Informe o Nome do Vendedor", "O valor informado � inv�lido. Por favor, tente novamente", valor);
        }
    }

    public class ListagemVendedores : IListagem<Domain.Vendedor>
    {
        public void Listar(IEnumerable<Domain.Vendedor> items)
        {
            var vendedores = items as Domain.Vendedor[] ?? items.ToArray();
            if (!vendedores.Any())
            {
                this.Imprime("N�o h� vendedores cadastrados.");
            }

            foreach (var vendedor in vendedores)
            {
                this.Imprime("Vendedor[Id:{0}; Nome: {1}; Cpf: {2}; Taxa de Comissao: {3} %]",
                    vendedor.VendedorId,
                    vendedor.Nome,
                    vendedor.Cpf,
                    vendedor.TaxaDeComissao * 100);
            }

            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Console.Clear();
        }
    }
}