﻿namespace Faculdade.Vendas2.ShellView
{
    public class MenuOption
    {
        public delegate void ProcessaMenu();

        public string Opcao { get; set; }

        public ProcessaMenu Metodo { get; set; }
    }
}