﻿using System;
using System.Windows.Forms;
using Faculdade.Vendas2.ShellView.Controller;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView
{
    class Program
    {
        // ReSharper disable once UnusedParameter.Local
        static void Main(string[] args)
        {
            var telaPrincipal = new HomeController();
            telaPrincipal.Carregar();

            Console.WriteLine("Pressione qualquer tecla para sair...");
            Console.ReadKey();
        }
    }
}
