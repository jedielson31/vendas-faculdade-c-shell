namespace Faculdade.Vendas2.ShellView.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        ClienteId = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        Cpf = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.ClienteId);
            
            CreateTable(
                "dbo.FormaDePagamento",
                c => new
                    {
                        FormaDePagamentoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.FormaDePagamentoId);
            
            CreateTable(
                "dbo.Produto",
                c => new
                    {
                        ProdutoId = c.Int(nullable: false, identity: true),
                        Nome = c.String(maxLength: 100, unicode: false),
                        PrecoDeCompra = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Markup = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.ProdutoId);
            
            CreateTable(
                "dbo.ProdutoVenda",
                c => new
                    {
                        ProdutoVendaId = c.Int(nullable: false, identity: true),
                        ProdutoId = c.Int(nullable: false),
                        Quantidade = c.Int(nullable: false),
                        Venda_VendaId = c.Int(),
                    })
                .PrimaryKey(t => t.ProdutoVendaId)
                .ForeignKey("dbo.Produto", t => t.ProdutoId)
                .ForeignKey("dbo.Venda", t => t.Venda_VendaId)
                .Index(t => t.ProdutoId)
                .Index(t => t.Venda_VendaId);
            
            CreateTable(
                "dbo.Venda",
                c => new
                    {
                        VendaId = c.Int(nullable: false, identity: true),
                        DataDaVenda = c.DateTime(nullable: false),
                        Cliente_ClienteId = c.Int(),
                        FormaDePagamento_FormaDePagamentoId = c.Int(),
                        Vendedor_VendedorId = c.Int(),
                    })
                .PrimaryKey(t => t.VendaId)
                .ForeignKey("dbo.Cliente", t => t.Cliente_ClienteId)
                .ForeignKey("dbo.FormaDePagamento", t => t.FormaDePagamento_FormaDePagamentoId)
                .ForeignKey("dbo.Vendedor", t => t.Vendedor_VendedorId)
                .Index(t => t.Cliente_ClienteId)
                .Index(t => t.FormaDePagamento_FormaDePagamentoId)
                .Index(t => t.Vendedor_VendedorId);
            
            CreateTable(
                "dbo.Vendedor",
                c => new
                    {
                        VendedorId = c.Int(nullable: false, identity: true),
                        TaxaDeComissao = c.Double(nullable: false),
                        Nome = c.String(maxLength: 100, unicode: false),
                        Cpf = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.VendedorId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Venda", "Vendedor_VendedorId", "dbo.Vendedor");
            DropForeignKey("dbo.ProdutoVenda", "Venda_VendaId", "dbo.Venda");
            DropForeignKey("dbo.Venda", "FormaDePagamento_FormaDePagamentoId", "dbo.FormaDePagamento");
            DropForeignKey("dbo.Venda", "Cliente_ClienteId", "dbo.Cliente");
            DropForeignKey("dbo.ProdutoVenda", "ProdutoId", "dbo.Produto");
            DropIndex("dbo.Venda", new[] { "Vendedor_VendedorId" });
            DropIndex("dbo.Venda", new[] { "FormaDePagamento_FormaDePagamentoId" });
            DropIndex("dbo.Venda", new[] { "Cliente_ClienteId" });
            DropIndex("dbo.ProdutoVenda", new[] { "Venda_VendaId" });
            DropIndex("dbo.ProdutoVenda", new[] { "ProdutoId" });
            DropTable("dbo.Vendedor");
            DropTable("dbo.Venda");
            DropTable("dbo.ProdutoVenda");
            DropTable("dbo.Produto");
            DropTable("dbo.FormaDePagamento");
            DropTable("dbo.Cliente");
        }
    }
}
