using Faculdade.Vendas2.ShellView.Domain;

namespace Faculdade.Vendas2.ShellView.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Faculdade.Vendas2.ShellView.Context.EFContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Faculdade.Vendas2.ShellView.Context.EFContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            context.Clientes
                .AddOrUpdate(new Cliente()
                {
                    Cpf = "3498513257",
                    Nome = "Cliente 1"
                }, new Cliente()
                {
                    Cpf = "8097123412",
                    Nome = "Cliente 2"
                }, new Cliente()
                {
                    Cpf = "347631621474",
                    Nome = "Cliente 3"
                }, new Cliente()
                {
                    Cpf = "3425987998",
                    Nome = "Cliente 4"
                });

            context.Vendedores
                .AddOrUpdate(new Vendedor
                {
                    Nome = "Vendedor 1",
                    Cpf = "234234777798",
                    TaxaDeComissao = 0.02
                },
                new Vendedor()
                {
                    Nome = "Vendedor 2",
                    Cpf = "2341293768",
                    TaxaDeComissao = 0.03
                },
                new Vendedor()
                {
                    Nome = "Vendedor 3",
                    Cpf = "38745932457",
                    TaxaDeComissao = 0.015
                },
                new Vendedor()
                {
                    Nome = "Vendedor 4",
                    Cpf = "23872134098",
                    TaxaDeComissao = 0.02
                },
                new Vendedor()
                {
                    Nome = "Vendedor 5",
                    Cpf = "123437979867",
                    TaxaDeComissao = 0.015
                });

            context.Produtos
                .AddOrUpdate(new Produto
                {
                    Nome = "Produto 1",
                    Markup = 0.30M,
                    PrecoDeCompra = 15.00M
                },
                new Produto()
                {
                    Nome = "Produto 2",
                    Markup = 0.85M,
                    PrecoDeCompra = 1.90M
                },
                new Produto()
                {
                    Nome = "Produto 3",
                    Markup = 0.47M,
                    PrecoDeCompra = 10.90M
                },
                new Produto()
                {
                    Nome = "Produto 4",
                    Markup = 0.30M,
                    PrecoDeCompra = 17.75M
                },
                new Produto()
                {
                    Nome = "Produto 5",
                    Markup = 0.50M,
                    PrecoDeCompra = 16.30M
                },
                new Produto()
                {
                    Nome = "Produto 6",
                    Markup = 0.15M,
                    PrecoDeCompra = 110.00M
                },
                new Produto()
                {
                    Nome = "Produto 7",
                    Markup = 0.18M,
                    PrecoDeCompra = 35.40M
                });

            context.FormasDePagamento
                .AddOrUpdate(new FormaDePagamento
                {
                    Nome = "Dinheiro"
                });
        }
    }
}
