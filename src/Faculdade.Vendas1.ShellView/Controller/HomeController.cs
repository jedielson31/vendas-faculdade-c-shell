﻿using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class HomeController : ControllerBase
    {
        protected override List<MenuOption> CarregaMenu()
        {
            return new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = AbreMenuClientes
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = AbreMenuProdutos,
                },
                new MenuOption
                {
                    Opcao = "3",
                    Metodo = AbreMenuVendedores
                },
                new MenuOption
                {
                    Opcao  = "4",
                    Metodo = AbreMenuFormaDePagamento
                },
                new MenuOption
                {
                    Opcao  = "5",
                    Metodo = AbreMenuVendas
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };
        }

        private void AbreMenuFormaDePagamento()
        {
            new FormaDePagamentoController().Carregar();
            Carregar();
        }

        private void Sair()
        {

        }

        private void AbreMenuProdutos()
        {
            new ProdutoController().Carregar();
            Carregar();
        }

        private void AbreMenuClientes()
        {
            new ClienteController().Carregar();
            Carregar();
        }

        private void AbreMenuVendedores()
        {
            new VendedorController().Carregar();
            Carregar();
        }

        private void AbreMenuVendas()
        {
            new VendaController().Carregar();
            Carregar();
        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU PRINCIPAL", false);
            this.Imprime("1 - Clientes", false);
            this.Imprime("2 - Produtos");
            this.Imprime("3 - Vendedor");
            this.Imprime("4 - Formas de Pagamento");
            this.Imprime("5 - Vendas");
            this.Imprime("0 - Sair");
        }
    }
}