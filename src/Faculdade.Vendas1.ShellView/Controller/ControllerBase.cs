﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public abstract class ControllerBase : ITela
    {
        protected readonly List<MenuOption> OpcoesMenu;

        protected ControllerBase()
        {
            // ReSharper disable once VirtualMemberCallInConstructor
            OpcoesMenu = CarregaMenu();
        }

        internal void Carregar()
        {
            ImprimeMenu();
            var opcaoMenu = LeOpcaoMenu();
            var option = OpcoesMenu.Find(x => x.Opcao == opcaoMenu);
            option.Metodo.Invoke();
        }

        protected abstract List<MenuOption> CarregaMenu();

        protected abstract void ImprimeMenu();

        protected virtual string LeOpcaoMenu()
        {
            var opcao = Console.ReadLine();
            while (OpcoesMenu.All(x => x.Opcao != opcao))
            {
                this.ImprimeErro("Opção Inválida. Tente Novamente");
                ImprimeMenu();
                opcao = Console.ReadLine();
            }

            Console.Clear();
            return opcao;
        }
    }

    public interface ITela
    {
    }
}