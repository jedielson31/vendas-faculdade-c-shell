﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Services;
using Faculdade.Vendas2.ShellView.View.Clientes;
using Faculdade.Vendas2.ShellView.View.Common;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class ClienteController : ControllerBase
    {

        private readonly ClienteService _service;

        public ClienteController()
        {
            _service = new ClienteService();
        }

        private void CadastrarClientes()
        {
            var form = new FormularioCadastroClientes();
            var cliente = form.Cadastrar();
            var result = _service.Create(cliente);

            while (!result.Success)
            {
                this.ImprimeResultadoComErros(result);
                cliente = form.Cadastrar(cliente);
                result = _service.Create(cliente);
            }

            this.ImprimeSucesso("Cliente Cadastrado com sucesso.");
            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Carregar();
        }

        private void ListarClientes()
        {
            var clientes = _service.All();
            new ListagemClientes().Listar(clientes);
            Carregar();
        }

        private void Sair()
        {
            
        }

        #region Overrides
        protected override List<MenuOption> CarregaMenu()
        {
            var opcoesMenu = new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = CadastrarClientes
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = ListarClientes
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };



            return opcoesMenu;
        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU CLIENTES", false);
            this.Imprime("1 - Cadastrar", false);
            this.Imprime("2 - Listar");
            this.Imprime("0 - Sair");
        }
        #endregion
    }
}