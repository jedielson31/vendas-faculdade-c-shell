﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Services;
using Faculdade.Vendas2.ShellView.View.Common;
using Faculdade.Vendas2.ShellView.View.FormasDePagamento;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class FormaDePagamentoController : ControllerBase
    {
        private readonly FormaDePagamentoService _service;

        public FormaDePagamentoController()
        {
            _service = new FormaDePagamentoService();
        }

        private void ListarTodos()
        {
            var formas = _service.All();
            new ListagemFormaDePagamento().Listar(formas);
            Carregar();
        }

        private void Cadastrar()
        {
            var form = new FormularioDeCadastroFormasDePagamento();
            var formaDePagamento = form.Cadastrar();
            var result = _service.Create(formaDePagamento);

            while (!result.Success)
            {
                this.ImprimeResultadoComErros(result);
                formaDePagamento = form.Cadastrar(formaDePagamento);
                result = _service.Create(formaDePagamento);
            }

            this.ImprimeSucesso("Forma de Pagamento cadastrada com sucesso.");
            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Carregar();
        }

        #region Overrides
        protected override List<MenuOption> CarregaMenu()
        {
            return new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = Cadastrar
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = ListarTodos
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };
        }

        private void Sair()
        {

        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU FORMAS DE PAGAMENTO", false);
            this.Imprime("1 - Cadastrar", false);
            this.Imprime("2 - Listar");
            this.Imprime("0 - Sair");
        } 
        #endregion
    }
}