﻿using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Services;
using Faculdade.Vendas2.ShellView.View.Common;
using Faculdade.Vendas2.ShellView.View.Venda;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class VendaController : ControllerBase
    {
        private readonly VendaService _service;
        private readonly ClienteService _clientesService;
        private readonly VendedorService _vendedoresService;
        private readonly ProdutoService _produtosService;
        private readonly FormaDePagamentoService _formaDePagamentoService;

        public VendaController()
        {
            _service = new VendaService();
            _clientesService = new ClienteService();
            _vendedoresService = new VendedorService();
            _produtosService = new ProdutoService();
            _formaDePagamentoService = new FormaDePagamentoService();
        }

        private void Listar()
        {
            var vendas = _service.All();
            new ListagemVendas().Listar(vendas);
            Carregar();
        }

        private void Cadastrar()
        {
            var vendas = new FormularioCadastroVendas(_clientesService.All().ToList(),
                _vendedoresService.All().ToList(),
                _produtosService.All().ToList(),
                _formaDePagamentoService.All().ToList()).CadastrarVendas();

            foreach (var venda in vendas)
            {
                var result = _service.Create(venda);

                if (!result.Success)
                {
                    this.ImprimeErro("Deu ruim ao salvar uma venda.");
                }

            }
            Carregar();
        }

        private void ListarPorData()
        {
            var dataInicial = this.LeData("Informe a data de inicio", "O valor informado é inválido. Por favor, tente novamente");
            var dataFinal = this.LeData("Informe a data final", "O valor informado é inválido. Por favor, tente novamente");

            var vendas = _service.All().Where(x => x.DataDaVenda >= dataInicial && x.DataDaVenda <= dataFinal).ToList();
            new ListagemVendas().Listar(vendas);

            Carregar();
        }

        private void ListarPorCliente()
        {
            var clientes = this.SelecionarListaDeClientes(_clientesService.All().ToList());
            var clientesId = clientes.Select(x => x.ClienteId).ToList();

            var vendas = _service.All().Where(x => clientesId.Any(t => t == x.VendaId)).ToList();
            new ListagemVendas().Listar(vendas);

            Carregar();
        }

        private void ListarPorFormaDePagamenxto()
        {
            var forma = this.SelecionarFormaDePagamento(_formaDePagamentoService.All().ToList());
            var vendas = _service.All().Where(x => x.FormaDePagamento.FormaDePagamentoId == forma.FormaDePagamentoId).ToList();
            new ListagemVendas().Listar(vendas);

            Carregar();
        }

        #region Overrides
        protected override List<MenuOption> CarregaMenu()
        {
            return new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = Cadastrar
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = Listar
                },
                new MenuOption
                {
                    Opcao = "3",
                    Metodo = ListarPorCliente
                },
                new MenuOption
                {
                    Opcao = "4",
                    Metodo = ListarPorData
                },
                new MenuOption
                {
                    Opcao = "5",
                    Metodo = ListarPorFormaDePagamenxto
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };
        }

        private void Sair()
        {

        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU VENDAS", false);
            this.Imprime("1 - Cadastrar", false);
            this.Imprime("2 - Listar Todas");
            this.Imprime("3 - Listar Por Cliente");
            this.Imprime("4 - Listar Por Data");
            this.Imprime("5 - Listar Por Forma de Pagamento");
            this.Imprime("0 - Sair");
        }
        #endregion
    }
}