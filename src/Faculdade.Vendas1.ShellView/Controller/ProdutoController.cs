﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Services;
using Faculdade.Vendas2.ShellView.View.Common;
using Faculdade.Vendas2.ShellView.View.Produtos;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class ProdutoController : ControllerBase
    {
        private readonly ProdutoService _service;
        public ProdutoController()
        {
            _service = new ProdutoService();
        }

        private void ListarProdutos()
        {
            var produtos = _service.All();
            new ListagemProdutos().Listar(produtos);
            Carregar();
        }

        private void CadastrarProduto()
        {
            var form = new FormularioCadastroProduto();
            var produto = form.Cadastrar();
            var result = _service.Create(produto);

            while (!result.Success)
            {
                this.ImprimeResultadoComErros(result);
                produto = form.Cadastrar(produto);
                result = _service.Create(produto);
            }

            this.ImprimeSucesso("Cliente Cadastrado com sucesso.");
            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Carregar();
        }

        private void Sair()
        {
            
        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU PRODUTOS", false);
            this.Imprime("1 - Cadastrar", false);
            this.Imprime("2 - Listar");
            this.Imprime("0 - Sair");
        }

        protected override List<MenuOption> CarregaMenu()
        {
            return new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = CadastrarProduto
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = ListarProdutos
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };
        }
    }
}