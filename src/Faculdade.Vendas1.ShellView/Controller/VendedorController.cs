﻿using System;
using System.Collections.Generic;
using Faculdade.Vendas2.ShellView.Services;
using Faculdade.Vendas2.ShellView.View.Common;
using Faculdade.Vendas2.ShellView.View.Vendedor;

namespace Faculdade.Vendas2.ShellView.Controller
{
    public class VendedorController : ControllerBase
    {
        private readonly VendedorService _service;
        public VendedorController()
        {
            _service = new VendedorService();
        }

        protected override List<MenuOption> CarregaMenu()
        {
            return new List<MenuOption>
            {
                new MenuOption
                {
                    Opcao = "1",
                    Metodo = CadastrarVendedor
                },
                new MenuOption
                {
                    Opcao = "2",
                    Metodo = ListarVendedores
                },
                new MenuOption
                {
                    Opcao = "0",
                    Metodo = Sair
                }
            };
        }

        private void Sair()
        {

        }

        private void ListarVendedores()
        {
            var vendedores = _service.All();
            new ListagemVendedores().Listar(vendedores);
            Carregar();
        }

        private void CadastrarVendedor()
        {
            var form = new FormularioCadastroVendedor();
            var vendedor = form.Cadastrar();
            var result = _service.Create(vendedor);

            while (!result.Success)
            {
                this.ImprimeResultadoComErros(result);
                vendedor = form.Cadastrar(vendedor);
                result = _service.Create(vendedor);
            }

            this.ImprimeSucesso("Vendedor Cadastrado com sucesso.");
            this.ImprimeAlerta("Pressione qualquer tecla para voltar ao menu...");
            Console.ReadKey();
            Carregar();
        }

        protected override void ImprimeMenu()
        {
            this.ImprimeAlerta("MENU VENDEDORES", false);
            this.Imprime("1 - Cadastrar", false);
            this.Imprime("2 - Listar");
            this.Imprime("0 - Sair");
        }
    }
}