﻿using System.Collections.Generic;

namespace Faculdade.Vendas2.ShellView.Repository.Interfaces
{
    public interface IRepository<TEntity>
    {
        IEnumerable<TEntity> All();
        void Create(TEntity entity);
    }
}