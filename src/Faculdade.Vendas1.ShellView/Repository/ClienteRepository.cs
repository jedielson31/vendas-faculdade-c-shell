﻿using Faculdade.Vendas2.ShellView.Domain;
using Faculdade.Vendas2.ShellView.Repository.Common;
using Faculdade.Vendas2.ShellView.Repository.Interfaces;

namespace Faculdade.Vendas2.ShellView.Repository
{
    public class ClienteRepository : RepositoryBase<Cliente>
    {
    }
}