﻿using System;
using System.Collections.Generic;
using System.Linq;
using Faculdade.Vendas2.ShellView.Context;
using Faculdade.Vendas2.ShellView.Repository.Interfaces;

namespace Faculdade.Vendas2.ShellView.Repository.Common
{
    public abstract class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected static EFContext Context { get; private set; }

        protected RepositoryBase()
        {
            if (Context == null)
            {
                Context = new EFContext();
            }
        }

        public IEnumerable<TEntity> All()
        {
            return Context.Set<TEntity>().ToList();
        }

        public virtual void Create(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentException(nameof(entity));
            }

            Context.Set<TEntity>().Add(entity);
            Context.SaveChanges();
        }
    }
}