﻿using System;
using System.Collections.Generic;

namespace Faculdade.Vendas2.ShellView.Domain
{
    public class Venda
    {
        public Venda()
        {
            ProdutosDaVenda = new List<ProdutoVenda>();
            Cliente = new Cliente();
            Vendedor = new Vendedor();
            FormaDePagamento = new FormaDePagamento();
        }

        public int VendaId { get; set; }

        public DateTime DataDaVenda { get; set; }

        public List<ProdutoVenda> ProdutosDaVenda { get; set; }

        public Cliente Cliente { get; set; }

        public Vendedor Vendedor { get; set; }

        public FormaDePagamento FormaDePagamento { get; set; }

    }
}