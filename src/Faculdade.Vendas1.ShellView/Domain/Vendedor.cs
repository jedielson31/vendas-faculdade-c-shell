﻿namespace Faculdade.Vendas2.ShellView.Domain
{
    public class Vendedor : Pessoa
    {
        public int VendedorId { get; set; }

        public double TaxaDeComissao { get; set; }
    }
}