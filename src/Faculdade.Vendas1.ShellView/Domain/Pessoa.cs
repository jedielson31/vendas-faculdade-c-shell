﻿namespace Faculdade.Vendas2.ShellView.Domain
{
    public abstract class Pessoa
    {
        public string Nome { get; set; }

        public string Cpf { get; set; }
    }
}
