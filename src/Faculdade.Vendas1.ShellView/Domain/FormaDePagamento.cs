﻿namespace Faculdade.Vendas2.ShellView.Domain
{
    public class FormaDePagamento
    {
        public int FormaDePagamentoId { get; set; }

        public string Nome { get; set; }
    }
}