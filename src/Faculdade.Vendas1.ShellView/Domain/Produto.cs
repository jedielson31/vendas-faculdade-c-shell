﻿using System;

namespace Faculdade.Vendas2.ShellView.Domain
{
    public class Produto
    {
        public int ProdutoId { get; set; }

        public string Nome { get; set; }

        public decimal PrecoDeCompra { get; set; }

        public decimal Markup { get; set; }

        public decimal ValorUnitario
        {
            get { return Math.Round(PrecoDeCompra*(decimal.One + Markup), 2); }
        }
    }
}