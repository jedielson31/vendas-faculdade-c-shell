﻿using System;

namespace Faculdade.Vendas2.ShellView.Domain
{
    public class ProdutoVenda
    {
        public ProdutoVenda()
        {
            Produto = new Produto();    
        }

        public int ProdutoVendaId { get; set; }

        public int ProdutoId { get; set; }

        public int Quantidade { get; set; }

        public Produto Produto { get; set; }

        public decimal SubTotal
        {
            get { return Math.Round(Quantidade* Produto.ValorUnitario, 2); }
        }
    }
}